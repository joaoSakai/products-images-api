package com.productsimages;

import com.productsimages.model.Product;
import com.productsimages.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgenari on 3/16/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void allProductsSizeListTest(){
        Iterable<Product> products = productService.getAllProducts();
        Assert.assertEquals(6, getProductsList(products).size());
    }

    @Test
    public void findProductByIdTest() {
        Product product = productService.getProductById(1l);
        Assert.assertNotNull(product);
        Assert.assertEquals("Smartphone", product.getName());
    }

    @Test
    public void findProductByNonExistentId() {
        Product product = productService.getProductById(10l);
        Assert.assertNull(product);
    }

    private List<Product> getProductsList(Iterable<Product> products) {
        List<Product> finalProducts = new ArrayList<Product>();
        for(Product prd : products) {
            finalProducts.add(new Product(prd.getId(), prd.getName(), prd.getDescription()));
        }
        return finalProducts;
    }
}
