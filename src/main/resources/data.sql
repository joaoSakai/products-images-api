--Image insertion
insert into Images (image_id, type) values(1, 'TV.jpg');
insert into Images (image_id, type) values(2, 'Smartphone.jpg');
insert into Images (image_id, type) values(3, 'Tablet.jpg');
insert into Images (image_id, type) values(4, 'Laptop.jpg');
insert into Images (image_id, type) values(5, 'Battery.jpg');

--Products insertion
insert into Product (id, name, description, product_id, image_id)
values (1, 'Smartphone', 'Personal Phone', null, 2);

insert into Product (id, name, description, product_id, image_id)
values (2, 'Tablet', 'Device that be used for leisure activities', null, 3);

insert into Product (id, name, description, product_id, image_id)
values (3, 'Laptop', 'Personal Computer', null, 4);

insert into Product (id, name, description, product_id, image_id)
values (4, 'TV', 'Device used to watch series and films', null, 1);

insert into Product (id, name, description, product_id, image_id)
values (5, 'Home Theater', 'Device used for improve sound effects', 4, null);

insert into Product (id, name, description, product_id, image_id)
values (6, 'Battery', 'Provide power to devices', 1, 5);


