package com.productsimages.repository;

import com.productsimages.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.ws.rs.Produces;
import java.util.List;

/**
 * Created by jgenari on 2/22/17.
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByName(final String name);

}
