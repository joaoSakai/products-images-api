package com.productsimages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsImagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsImagesApplication.class, args);
	}
}
