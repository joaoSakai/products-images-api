package com.productsimages.configuration;

import com.productsimages.controller.ProductController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jgenari on 2/22/17.
 */
@Configuration
public class JerseyConfig extends ResourceConfig{

    public JerseyConfig() {
        register(ProductController.class);
    }
}
