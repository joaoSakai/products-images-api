package com.productsimages.controller;

import com.productsimages.model.Images;
import com.productsimages.model.Product;
import com.productsimages.service.ProductService;
import jersey.repackaged.com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgenari on 2/22/17.
 */

@Component
@Path("/products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {

    private static final String CHILDS = "childs";
    private static final String IMAGES = "images";

    @Autowired
    private ProductService productService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> allProducts(@QueryParam("type") String type) {
        Iterable<Product> products = productService.getAllProducts();
        List<Product> finalProducts = new ArrayList<Product>();

        if(type == null) {
            for(Product prd : products) {
                if (prd.getChildProducts() == null || prd.getImage() == null) {
                    finalProducts.add(new Product(prd.getId(), prd.getName(), prd.getDescription()));
                }
            }
            return finalProducts;
        }
        switch (type) {
            case CHILDS:
                for(Product prd : products) {
                    if(prd.getChildProducts() != null) {
                        finalProducts.add(new Product(prd.getId(), prd.getName(), prd.getDescription(), prd.getChildProducts(), null));
                    }
                }
                break;
            case IMAGES:
                for(Product prd : products) {
                    if(prd.getImage() != null) {
                        finalProducts.add(new Product(prd.getId(), prd.getName(), prd.getDescription(), null, prd.getImage()));
                    }
                }
                break;
        }
        return finalProducts;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Product productById(@PathParam("id") long id, @QueryParam("type") String type) {
        Product product = productService.getProductById(id);
        if(product != null) {
            if(type == null) {
                return product;
            }
            switch (type) {
                case CHILDS:
                    if(product.getChildProducts() != null) {
                        return product;
                    }
                    break;
                case IMAGES:
                    if(product.getImage() != null) {
                        return product;
                    }
                    break;
            }
        }
        return null;
    }

    @GET
    @Path("{id}/images")
    public Images getImage(@PathParam("id") Long id, final Images images) {
        if (id != null) {
            Product parentProduct = productService.getProductById(id);
            return parentProduct.getImage();
        }
        return null;
    }

    @GET
    @Path("/childs")
    public List<Product> getChildProducts(@PathParam("id") Long id) {
        if (id != null) {
            Product parentProduct = productService.getProductById(id);
            return parentProduct.getChildProducts();
        }
        return null;
    }



    @POST
    @Path("{id}")
    public Product addChildProductProduct(@PathParam("id") Long id, final Product childProduct) {
        if (id != null) {
            Product parentProduct = productService.getProductById(id);
            parentProduct.addChildProduct(childProduct);

            if(childProduct != null) {
                productService.save(parentProduct);
                return parentProduct;
            }
        }

        return null;
    }

    @POST
    @Path("{id}/images")
    public Product addImage(@PathParam("id") Long id, final Images images) {
        if (id != null) {
            Product parentProduct = productService.getProductById(id);
            parentProduct.setImage(images);

            if(images != null) {
                productService.save(parentProduct);
                return parentProduct;
            }
        }

        return null;
    }

}
