package com.productsimages.service;

import com.productsimages.repository.ProductRepository;
import com.productsimages.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jgenari on 2/22/17.
 */
@Service("productService")
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void save(final Product product) {
        productRepository.save(product);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }


    public Product getProductById(final long id) {
        return productRepository.findOne(id);
    }
}
