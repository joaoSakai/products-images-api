# README #

Avenue Hiring process project: Product and Images API
Author: @joaoSakai

# **How to use the ProductsImageApi** #

To run the API execute **mvn clean spring-boot:run** command on project root directory.
This command will run all tests and after that up the application (see the image below);

![Screenshot from 2017-03-16 21-05-36.png](https://bitbucket.org/repo/8zjggeA/images/1897216921-Screenshot%20from%202017-03-16%2021-05-36.png)

# API Specification  #

### To run all API test you can use the [Postman APP](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) ###

## Product ##

GET    /products    
get all products without relationships (childProducts / images);

GET    /products?type=childs    
get all products with child products;

GET    /products?type=images
get all products with images;

GET    /products/{id}
get specific product by id

GET    /products/{id}?type=childs    
get specific product by id with child products;

GET    /products/{id}?type=images    
get specific product by id with image;

GET    /products/{id}/images    
get image related to the product;

GET    /products/{id}/childs         
get all child products related to the product;

POST   /products/{id}           
add child Product into specific product

Example:
```
#!json

{
  "id": 8,
  "name": "Sound Remote Control",
  "description": "System of remote control sound effects",
  "childProducts": [],
  "image": null
}
```
 
POST   /products/{id}/images    
update the product image
Example
```
#!json
{
  "image": {
    "id": 8,
    "type": "HomeTheater.jpg"
  }
}
```